const express = require('express')
const app = express();

// app.set('view engine','ejs')
// app.set('view', './view')

app.get('/',(req,res)=>{
    res.sendFile('./view/Acceuille.html', {root:__dirname})
    // res.render('About.ejs')
})
app.get('/about',(req,res)=>{
    res.sendFile('./view/About.html', {root:__dirname})
    // res.render('About.ejs')
})
app.get('/nosservice',(req,res)=>{
    res.sendFile('./view/NosService.html', {root:__dirname})
    // res.render('About.ejs')
})
app.listen(3001, ()=>{
    console.log('http://localhost:3001')
})